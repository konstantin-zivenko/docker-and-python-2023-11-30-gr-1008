from psycopg2 import OperationalError, connect

from bot.src.settings import settings


def create_connection(
    db_name=settings.DB_DBNAME,
    db_user=settings.DB_USER,
    db_password=settings.DP_PASSWORD,
    db_host=settings.DB_HOST,
    db_port=settings.DB_PORT
):
    try:
        print("try connection...")
        connection = connect(
            database=db_name,
            user=db_user,
            password=db_password,
            host=db_host,
            port=db_port,
        )
        print("Connection to PostgreSQL DB successful")
    except OperationalError as e:
        print(f"The error '{e}' occurred")
        connection = None
    return connection


conn = create_connection()
