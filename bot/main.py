import sys
import asyncio
import logging

from bot.src.echobot import main

logging.basicConfig(level=logging.INFO, stream=sys.stdout)
asyncio.run(main())
